import { Helmet } from 'react-helmet';
import Header from '../../Components/Header/Header';

const ExpensesCategory = () => {
    const title = 'Gastos por categoría';

    return (
        <>
            <Helmet>
                <title>Gastos por categoría</title>
            </Helmet>

            <Header title={title}/>
        </>
    );
}

export default ExpensesCategory;