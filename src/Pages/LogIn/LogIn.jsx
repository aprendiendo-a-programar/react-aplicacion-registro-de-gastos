import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { ReactComponent as SvgLogin } from '../../Img/login.svg';
import { auth } from '../../Firebase/FirebaseConfig';
import { useHistory } from 'react-router-dom';
import AlertRight from '../../Components/AlertRight/AlertRight';
import AlertError from '../../Components/AlertError/AlertError';


const LogInt = () => {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [alertRightState, changeAlertRightState] = useState(false);
    const [alertErrorState, changeAlertErrorState] = useState(false);
    const [alert, changeAlert] = useState({});

    const handleChange = (e) => {
        if (e.target.name === 'email') {
            setEmail(e.target.value);
        } else if (e.target.name === 'password') {
            setPassword(e.target.value);
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        changeAlertRightState(false);
        changeAlertErrorState(false)
        changeAlert({});

        // Comprobar que el correo sea valido
        const validateEmail = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/;
        if (!validateEmail.test(email)) {
            changeAlertErrorState(true)
            changeAlert({
                message: 'Por favor introduce un email valido'
            });
            return;
        }

        if (email === '' || password === '') {
            changeAlertErrorState(true)
            changeAlert({
                message: 'Por favor rellena todos los campos'
            });
            return;
        }

        try {
            await auth.signInWithEmailAndPassword(email, password);
            changeAlertRightState(true)
            changeAlert({
                message: 'Iniciaste sesión con exito'
            });

            setTimeout(() => {
                history.push('/');
            }, 2000);

        } catch (error) {
            changeAlertErrorState(true);

            let message;
            switch (error.code) {
                case 'auth/user-not-found':
                    message = 'Tu email no es valido'
                    break;
                case 'auth/wrong-password':
                    message = 'La contraseña no es correcta'
                    break;
                default:
                    message = 'Hubo un error al intentar iniciar sesión.'
                    break;
            }

            changeAlert({ message: message });
        }
    }

    return (
        <>
            <Helmet>
                <title>Iniciar Sesión</title>
            </Helmet>

            <header className="header">
                <h1 className="header_title">Iniciar Sesión</h1>
                <div className="header__buttons">
                    <Link to="/crear-cuenta" className="header__buttons-single button-secondary">Crear cuenta</Link>
                </div>
            </header>

            <form action="" className="form" onSubmit={handleSubmit}>
                <SvgLogin className="form__svg" />
                <input
                    className="form__input"
                    type="email"
                    name="email"
                    placeholder="Correo Electrónico"
                    value={email}
                    onChange={handleChange}
                />
                <input
                    className="form__input"
                    type="password"
                    name="password"
                    placeholder="Contraseña"
                    value={password}
                    onChange={handleChange}
                />
                <div className="form__button ">
                    <button
                        className="button-primary"
                    >
                        Iniciar Sesión
                    </button>
                </div>

            </form>

            <AlertRight
                alertRight={alertRightState}
                message={alert.message}
                changeAlertRightState={changeAlertRightState}
            />

            <AlertError
                alertError={alertErrorState}
                message={alert.message}
                changeAlertErrorState={changeAlertErrorState}
            />
        </>
    );
}

export default LogInt;