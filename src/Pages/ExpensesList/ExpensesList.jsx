import { Helmet } from 'react-helmet';
import Header from '../../Components/Header/Header';
//import { useAuth } from '../../Context/AuthContext';

const ExpensesList = () => {
    const title = 'Lista de gastos';

    // const { user } = useAuth();
    

    return (
        <>
            <Helmet>
                <title>Lista de gastos</title>
            </Helmet>

            <Header title={title} />
        </>
    );
}

export default ExpensesList;