import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { ReactComponent as SvgLogin } from '../../Img/registro.svg';
import { auth } from '../../Firebase/FirebaseConfig';
import { useHistory } from 'react-router-dom';
import AlertRight from '../../Components/AlertRight/AlertRight';
import AlertError from '../../Components/AlertError/AlertError';

const UserRegister = () => {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [alertRightState, changeAlertRightState] = useState(false);
    const [alertErrorState, changeAlertErrorState] = useState(false);
    const [alert, changeAlert] = useState({});

    const handleChange = (e) => {
        switch (e.target.name) {
            case 'email':
                setEmail(e.target.value);
                break;
            case 'password':
                setPassword(e.target.value);
                break;
            case 'password2':
                setPassword2(e.target.value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        changeAlertRightState(false);
        changeAlertErrorState(false)
        changeAlert({});

        // Comprobar que el correo sea valido
        const validateEmail = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/;
        if (!validateEmail.test(email)) {
            changeAlertErrorState(true)
            changeAlert({
                message: 'Por favor introduce un email valido'
            });
            return;
        }

        if (email === '' || password === '' || password2 === '') {
            changeAlertErrorState(true)
            changeAlert({
                message: 'Por favor rellena todos los campos'
            });
            return;
        }

        if (password !== password2) {
            changeAlertErrorState(true)
            changeAlert({
                message: 'Las contraseñas no coinciden'
            });
            return;
        }

        try {
            await auth.createUserWithEmailAndPassword(email, password);
            changeAlertRightState(true)
            changeAlert({
                message: 'Registro con exito'
            });
            setTimeout(() => {
                history.push('/');
            }, 2000);
        } catch (error) {
            changeAlertErrorState(true)

            let message;
            switch (error.code) {
                case 'auth/invalid-password':
                    message = 'La contraseña tiene que ser de al menos 6 caracteres.'
                    break;
                case 'auth/email-already-in-use':
                    message = 'Ya existe una cuenta con el correo electrónico proporcionado.'
                    break;
                case 'auth/invalid-email':
                    message = 'El correo electrónico no es válido.'
                    break;
                default:
                    message = 'Hubo un error al intentar crear la cuenta.'
                    break;
            }

            changeAlert({ message: message });
        }
    }

    return (
        <>
            <Helmet>
                <title>Crear Cuenta</title>
            </Helmet>

            <header className="header">
                <h1 className="header_title">Crear Cuenta</h1>
                <div className="header__buttons">
                    <Link to="/iniciar-sesion" className="header__buttons-single button-secondary">Iniciar Sesión</Link>
                </div>
            </header>

            <form action="" className="form" onSubmit={handleSubmit}>
                <SvgLogin className="form__svg" />
                <input
                    className="form__input"
                    type="email"
                    name="email"
                    placeholder="Correo Electrónico"
                    value={email}
                    onChange={handleChange}
                />
                <input
                    className="form__input"
                    type="password"
                    name="password"
                    placeholder="Contraseña"
                    value={password}
                    onChange={handleChange}
                />
                <input
                    className="form__input"
                    type="password"
                    name="password2"
                    placeholder="Repetir Contraseña"
                    value={password2}
                    onChange={handleChange}
                />
                <div className="form__button ">
                    <button
                        className="button-primary"
                    >
                        Crear Cuenta
                    </button>
                </div>
            </form>

            <AlertRight
                alertRight={alertRightState}
                message={alert.message}
                changeAlertRightState={changeAlertRightState}
            />

            <AlertError
                alertError={alertErrorState}
                message={alert.message}
                changeAlertErrorState={changeAlertErrorState}
            />
        </>
    );
}

export default UserRegister;