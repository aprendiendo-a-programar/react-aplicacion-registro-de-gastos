import React, { useState, useContext, useEffect } from 'react';
import { auth } from '../Firebase/FirebaseConfig';

// Creamos el contexto
const AuthContext = React.createContext();

// Hook para acceder al contexto
const useAuth = () => {
    return useContext(AuthContext)
}

const AuthProvider = ({ children }) => {
    const [user, setUser] = useState();
    // Creamos un state para saber cuando termina de cargar la comprobación de onAuthStateChanged
    const [loading, setLoading] = useState(true);

    // Efecto para ejecutar la comprobacion una sola vez
    useEffect(() => {
        // Comprobar si hay usuario
        const cancelSuscription = auth.onAuthStateChanged((user) => {
            setUser(user);
            setLoading(false);
        });

        return cancelSuscription;
    }, []);

    // Solamente retornamos los elementos hijos cuando setLoading no este cargado.
    return (
        <AuthContext.Provider value={{ user: user }}>
            {!loading && children}
        </AuthContext.Provider>
    );
}

export { AuthProvider, AuthContext, useAuth };