import React from 'react';
import ReactDOM from 'react-dom';
// COMPONENTS
import App from './App';
import ExpensesCategory from './Pages/ExpensesCategory/ExpensesCategory';
import ExpensesList from './Pages/ExpensesList/ExpensesList';
import UserRegister from './Pages/UserRegister/UserRegister';
import LogIn from './Pages/LogIn/LogIn';
import { AuthProvider } from './Context/AuthContext';
import ExpensesEdit from './Components/ExpensesEdit/ExpensesEdit';
import RoutesPrivate from './Components/RoutePrivate/RoutePrivate';

import WebFont from 'webfontloader';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';
// CSS
import './index.scss';
import './Styles/Global.scss';
// IMG 
import favicon from './Img/logo.png';
import FondoPuntos from './Img/puntos.svg';

WebFont.load({
  google: {
    families: ['Work Sans:400, 500, 700', 'sans-serif']
  }
});

ReactDOM.render(
  <React.StrictMode>
    <>
      <Helmet>
        <link rel="shortcut icon" href={favicon} type="image/x-icon" />
      </Helmet>
      
      <AuthProvider>
        <BrowserRouter>
          <div className="main-container">
            <Switch>

              <Route path="/iniciar-sesion" component={LogIn} />
              <Route path="/crear-cuenta" component={UserRegister} />

              <RoutesPrivate path="/categorias">
                  <ExpensesCategory />
              </RoutesPrivate>

              <RoutesPrivate path="/lista">
                  <ExpensesList />
              </RoutesPrivate>

              <RoutesPrivate path="/editar/:id">
                  <ExpensesEdit />
              </RoutesPrivate>

              <RoutesPrivate path="/">
                  <App />
              </RoutesPrivate>

            </Switch>
          </div>
        </BrowserRouter>
      </AuthProvider>

      <img src={FondoPuntos} className="points-up" alt="puntos fondo" />
      <svg className="svg-main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" preserveAspectRatio="none">
        <path fill="#0099ff" d="M0,0L24,32C48,64,96,128,144,165.3C192,203,240,213,288,218.7C336,224,384,224,432,224C480,224,528,224,576,202.7C624,181,672,
        139,720,112C768,85,816,75,864,96C912,117,960,171,1008,186.7C1056,203,1104,181,1152,165.3C1200,149,1248,139,1296,133.3C1344,128,1392,128,1416,128L1440,128L1440,
        320L1416,320C1392,320,1344,320,1296,320C1248,320,1200,320,1152,320C1104,320,1056,320,1008,320C960,320,912,320,864,320C816,320,768,320,720,320C672,320,624,320,576,
        320C528,320,480,320,432,320C384,320,336,320,288,320C240,320,192,320,144,320C96,320,48,320,24,320L0,320Z">
        </path>
      </svg>
      <img src={FondoPuntos} className="points-down" alt="puntos fondo" />
    </>
  </React.StrictMode>,
  document.getElementById('root')
);
