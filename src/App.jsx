import './App.scss';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import BtnClose from './Components/BtnClose/BtnClose';
import ExpensesForm from './Components/ExpensesForm/ExpensesForm';

const App = () => {
  return (
    <div className="app">
      <>
        <Helmet>
          <title>Agregar gasto</title>
        </Helmet>
        
        <header className="header">
          <h1 className="header__title">Agregar gasto</h1>
          <div className="header__buttons">
            <Link to="/categorias" className="header__buttons-single button-secondary">Categorías</Link>
            <Link to="/lista" className="buttoms__buttons-single button-secondary">Lista de Gastos</Link>
            <BtnClose/>
          </div>
        </header>

        <ExpensesForm />

      </>
    </div>
  );
}
 
export default App;