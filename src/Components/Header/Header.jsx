import BtnBack from '../BtnBack/BtnBack';

const Header = ({title}) => {
    return (
        <header className="header">
            <BtnBack />
            <h1 className="header__title">{title}</h1>
        </header>
    );
}

export default Header;