import {ReactComponent as BtnCloseIcon} from '../../Img/log-out.svg';
import './BtnClose.scss';
import { auth } from '../../Firebase/FirebaseConfig';
import { useHistory } from 'react-router-dom';

const BtnClose = () => {
    const history = useHistory();

    const closeSession = async () => {
        try {
            await auth.signOut();
            history.push('/iniciar-sesion')
        } catch(error) {
            console.log(error)
        }
    }

    return (
        <button 
            className="btn-close"
            onClick={closeSession}
        >
            <BtnCloseIcon className="btn-icon"/>
        </button>
    );
}
 
export default BtnClose;