import React, {useEffect} from 'react';
import './AlertError.scss';

const Alertas = ({alertError, changeAlertErrorState, message}) => {
    useEffect(() => {
        let time;

        if(alertError === true) {
            time = setTimeout(() =>  {
                changeAlertErrorState(false);
            }, 4000);
        }

        return(() => clearTimeout(time));

    }, [alertError, changeAlertErrorState])

    return (
        <>
            {alertError &&
                <div className="alert-error">
                    <p className="alert-error__message">{message}</p>
                </div>
            }
        </>
    );
}

export default Alertas;