import React, { useEffect } from 'react';
import './AlertRight.scss'

const Alertas = ({alertRight, changeAlertRightState, message}) => {
    useEffect(() => {
        let time;

        if(alertRight === true) {
            time = setTimeout(() =>  {
                changeAlertRightState(false);
            }, 4000);
        }

        return(() => clearTimeout(time));

    }, [alertRight, changeAlertRightState])

    return (
        <>
            {alertRight && 
                <div className="alert-right">
                    <p className="alert-right__message">{message}</p>
                </div>
            }
        </>
    );
}

export default Alertas;