import {ReactComponent as IconBack} from '../../Img/flecha.svg'
import { useHistory } from 'react-router-dom';


import './BtnBack.scss'

const BtnBack = ({route = '/'}) => {
    const history = useHistory();

    return (
        <button 
            className="btn-back"
            onClick={() => history.push(route)}
        >
                <IconBack className="btn-icon"/>
        </button>
    );
}
 
export default BtnBack;