import { useAuth } from '../../Context/AuthContext';
import { Route, Redirect } from 'react-router-dom';

const RoutesPrivate = ({children, ...restOfProperties}) => {
    const {user } = useAuth();

    if(user) {
        return <Route {...restOfProperties}>{children}</Route>
    } else {
        return <Redirect to="/iniciar-sesion" />
    }
}
 
export default RoutesPrivate;