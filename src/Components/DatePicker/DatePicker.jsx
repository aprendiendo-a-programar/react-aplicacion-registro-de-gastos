import React from 'react';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import './DatePicker.scss'

import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';
import { es } from 'date-fns/locale';

function parseDate(str, format) {
  const parsed = dateFnsParse(str, format, new Date(), { locale: es });
  if (DateUtils.isDate(parsed)) {
    return parsed;
  }
  return undefined;
}

function formatDate(date, format) {
  return dateFnsFormat(date, format, { locale: es });
}

const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',];
const dias_semana_cortos = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];

const DatePicker = ({date, setDate}) => {
    return (
        <DayPickerInput
            value={date}
            onDateChange={(day) => setDate(day)}
            format="dd 'de' MMMM 'de' yyyy"
            parseDate={parseDate}
            formatDate={formatDate}

            dayPickerProps={
                {
                    month: meses,
                    weekdaysShort: dias_semana_cortos
                }
            }
        />
    );
}
 
export default DatePicker;