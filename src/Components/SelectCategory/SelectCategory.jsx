import React, { useState } from 'react';
import { ReactComponent as IconoDown } from '../../Img/down.svg';
import CategoryIcons from '../CategoryIcons/CategoryIcons';
import './SelectCategory.scss';

const SelecCategory = ({category, setCategory}) => {
    const [showSelect, setShowSelect] = useState(false);

    const categories = [
        {id: 'comida', text: 'Comida'},
        {id: 'cuentas y pagos', text: 'Cuentas y pagos'},
        {id: 'hogar', text: 'Hogar'},
        {id: 'transporte', text: 'Transporte'},
        {id: 'ropa', text: 'Ropa'},
        {id: 'salud e higiene', text: 'Salud e Higiene'},
        {id: 'compras', text: 'Compras'},
        {id: 'diversion', text: 'Diversion'}
    ]

    const handleClick = (e) => {
        setCategory(e.currentTarget.dataset.value);
    }
    

    return (
        <div className="select-cont" onClick={() => setShowSelect(!showSelect)}>

            <div className="select-cont__option-selected">
                {category} <IconoDown />
            </div>

            {
                showSelect && 
                    <div className="select-cont__options">
                        {categories.map((category) => {
                            return (
                                <div
                                    key={category.id}
                                    data-value={category.id}
                                    className="select-cont__option"
                                    onClick={handleClick}
                                >
                                    <CategoryIcons id={category.id}/>
                                    {category.text}
                                </div>
                            )
                        })}
                    </div>
            }

        </div>
    );
}
 
export default SelecCategory;