import React, { useState } from 'react'
import { ReactComponent as SvgPlus } from '../../Img/plus.svg';
import SelectCategory from '../SelectCategory/SelectCategory';
import DatePicker from '../DatePicker/DatePicker';
import './ExpensesForm.scss';

const ExpensesForm = () => {
    const [description, setDescription] = useState('');
    const [quantity, setQuantity] = useState('');
    const [category, setCategory] = useState('hogar');
    const [date, setDate] = useState(new Date());

    const handleChange = ((e) => {
        if(e.target.name === 'description') {
            setDescription(e.target.value);
        } else if(e.target.name === 'quantity') {
            setQuantity(e.target.value.replace(/[^0-9.]/g, ''));
        }
    })

    return (
        <form action="" className="form" >

                <div className="form__cat-dat">
                    <SelectCategory 
                        category={category}
                        setCategory={setCategory}
                    />
                    <DatePicker date={date} setDate={setDate}/>
                </div>

                <input
                    className="form__input"
                    type="text"
                    name="description"
                    id="description"
                    placeholder="Descripción"
                    value={description}
                    onChange={handleChange}
                />
                <input
                    className="form__input"
                    type="text"
                    name="quantity"
                    placeholder="0.00 €"
                    value={quantity}
                    onChange={handleChange}
                />

                <div className="form__button ">
                    <button
                        className="button-primary"
                        type="submit"
                    >
                        Agregar gasto <SvgPlus className="plusIcon"/>
                    </button>
                </div>
            </form>
    );
}
 
export default ExpensesForm;