import React from 'react'

import {ReactComponent as IconoComida} from '../../Img/cat_comida.svg';
import {ReactComponent as IconoCompras} from '../../Img/cat_compras.svg';
import {ReactComponent as IconoCuentasYPagos} from '../../Img/cat_cuentas-y-pagos.svg';
import {ReactComponent as IconoDiversion} from '../../Img/cat_diversion.svg';
import {ReactComponent as IconoHogar} from '../../Img/cat_hogar.svg';
import {ReactComponent as IconoRopa} from '../../Img/cat_ropa.svg';
import {ReactComponent as IconoSaludEHigiene} from '../../Img/cat_salud-e-higiene.svg';
import {ReactComponent as IconoTransporte} from '../../Img/cat_transporte.svg';

const CategoryIcons = ({id}) => {
    switch(id){
        case 'comida':
            return <IconoComida />
        case 'compras':
            return <IconoCompras />
        case 'cuentas y pagos':
            return <IconoCuentasYPagos />
        case 'diversion':
            return <IconoDiversion />
        case 'hogar':
            return <IconoHogar />
        case 'ropa':
            return <IconoRopa />
        case 'salud e higiene':
            return <IconoSaludEHigiene />
        case 'transporte':
            return <IconoTransporte />
        default:
            break;
    }
}
 
export default CategoryIcons;